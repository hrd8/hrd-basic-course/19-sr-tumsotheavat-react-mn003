import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { deleteCategory, fetchCategory, postCategory, updateCategory } from "../redux/action/categoryAction";
import { Container, Form, Table, Button } from "react-bootstrap";
import { strings } from "../localization/localize";
import { bindActionCreators } from "redux";
import sl from 'sweetalert2'

export default function Category() {
    const [isUpdate, setIsUpdate] = useState(null);
    const [newCategory, setNewCategory] = useState(null);
    const [isRefresh, setIsRefresh] = useState(false);

    const dispatch = useDispatch();
    const state = useSelector((states) => states.categoryReducer.categories);
    const onDelete = bindActionCreators(deleteCategory, dispatch);
    const onPostCategory = bindActionCreators(postCategory, dispatch);
    const onUpdateCategory = bindActionCreators(updateCategory, dispatch);

    useEffect(() => {
        dispatch(fetchCategory());
        setIsRefresh(false)
    }, [isRefresh,dispatch]);

    const onAdd = async (e) => {
        e.preventDefault();
        let category1 = { name: newCategory };
        onPostCategory(category1).then(message => {
            setIsRefresh(true)
            sl.fire({
                position: 'top',
                icon: 'success',
                title: message.payload,
                showConfirmButton: false,
                timer: 1500
            })
        });
        document.getElementById("text").value = "";
    };

    const onEdit = (id, name) => {
        setIsUpdate(id);
        setNewCategory(name);
        document.getElementById("text").value = name;
    };

    const onUpdate = async (e) => {
        e.preventDefault();
        let category1 = { name: newCategory };
        onUpdateCategory(isUpdate, category1).then(message => {
            setIsRefresh(true)
            sl.fire({
                position: 'top',
                icon: 'success',
                title: message.payload,
                showConfirmButton: false,
                timer: 1500
            })
        });
        setIsUpdate(null);
        document.getElementById("text").value = "";
    };

    return (
        <div>
            <Container>
                <h1 className="my-3">{strings.Category}</h1>
                <Form>
                    <Form.Group>
                        <Form.Label>{strings.Category}</Form.Label>
                        <Form.Control
                            id="text"
                            type="text"
                            placeholder="Category Name"
                            onChange={(e) => setNewCategory(e.target.value)}
                        />
                        <br />
                        <Button
                            type="submit"
                            variant="secondary"
                            onClick={isUpdate !== null ? onUpdate : onAdd}
                        >
                            {isUpdate !== null ? strings.Save : strings.Add}
                        </Button>
                        <Form.Text className="text-muted"></Form.Text>
                    </Form.Group>
                </Form>
                <Table striped bordered hover>
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>{strings.Name}</th>
                        <th>{strings.Action}</th>
                    </tr>
                    </thead>
                    <tbody>
                    {state.map((item, index) => (
                        <tr key={index}>
                            <td>{index + 1}</td>
                            <td>{item.name}</td>
                            <td>
                                <Button
                                    size="sm"
                                    variant="warning"
                                    onClick={()=>onEdit(item._id, item.name)}
                                >
                                    {strings.Edit}
                                </Button>{" "}
                                <Button
                                    size="sm"
                                    variant="danger"
                                    onClick={() => onDelete(item._id)}
                                >
                                    {strings.Delete}
                                </Button>
                            </td>
                        </tr>
                    ))}
                    </tbody>
                </Table>
            </Container>
        </div>
    );
}

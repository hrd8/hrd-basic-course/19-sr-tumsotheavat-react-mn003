import './App.css';
import { Switch, BrowserRouter, Route } from 'react-router-dom';
import { useState } from 'react';
import { LangContext } from './contexts/LangContext';
import MyNavBar from './components/MyNavBar';
import Article from './view/Article';
import Author from './view/Author';
import Category from './view/Category';
import Home from './view/Home';
import ViewArticle from './components/ViewArticle';


function App() {

  const [lang, setLang] = useState()

  return (
    <div>  
      <LangContext.Provider value={{ lang, setLang }}>
      <BrowserRouter>
        <MyNavBar/>
        <Switch>
          <Route exact path='/' component={Home}/>
          <Route path='/article' component={Article}/>
          <Route path='/author' component={Author}/>
          <Route path='/category' component={Category}/>
          <Route path='/view/:id' component={ViewArticle}/>
          <Route path='/update/article/:id' component={Article} />
        </Switch>
      </BrowserRouter>
      </LangContext.Provider>
    </div>
  );
}

export default App;

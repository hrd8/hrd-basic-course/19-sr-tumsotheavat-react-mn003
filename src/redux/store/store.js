import { applyMiddleware, createStore } from "redux";
import logger from "redux-logger";
import thunk from "redux-thunk";
import combinedReducer from "../reducer/combinedReducer";

export const store = createStore(combinedReducer, applyMiddleware(thunk, logger))